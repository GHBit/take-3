﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DisableMovementScript : MonoBehaviour {

    MonoBehaviour objectMovementScript;

	// Use this for initialization
	void Start () {
        objectMovementScript = gameObject.GetComponent<MovementScript>(); //gets the object's movement script
	}
	
	// Update is called once per frame
    // Simple switch that turns object's movement on or off
    // Also has a separate switch that turns off the whole object
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
            objectMovementScript.enabled = !objectMovementScript.enabled;

        if (Input.GetKeyDown(KeyCode.Q))
            gameObject.SetActive(false);
	}
}
