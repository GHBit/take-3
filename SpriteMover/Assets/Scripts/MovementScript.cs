﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{

    public float speed;
    public float alternateSpeed;
    private bool isUsingAlternateMovement = false;
    public KeyCode doAlternateMovement;
    public KeyCode doRecenter;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(doAlternateMovement))
            isUsingAlternateMovement = true;
        if (Input.GetKeyUp(doAlternateMovement))
            isUsingAlternateMovement = false;

        if (isUsingAlternateMovement && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
        {
            AlternateMovement();
            isUsingAlternateMovement = false;
        }
        else if (!Input.GetKey(doAlternateMovement))
        {
            DefaultMovement();
        }

        if (Input.GetKeyDown(doRecenter))
        {
            transform.position = new Vector3(0, 0, 0); //resets player position to origin
        }
    }

    //alternative movement script. Moves a different distance, but only once after pressing space
    private void AlternateMovement()
    {
        float directionX = Input.GetAxis("Horizontal") * alternateSpeed;
        float directionY = Input.GetAxis("Vertical") * alternateSpeed;
        directionX *= Time.deltaTime;
        directionY *= Time.deltaTime;
        transform.Translate(directionX, directionY, 0); //0 could theoretically be swapped for a directionZ
    }

    private void DefaultMovement()
    {
        //Movement Script. Works by getting an input axis and multiplying by speed for movement along both the X and Y axis.
        //deltaTime apparently makes the method independant of FPS.
        //https://docs.unity3d.com/ScriptReference/Input.GetAxis.html used as reference material for GetAxis and deltaTime.
        //De-Commenting float directionY = 1 * speed; and commenting the line below it makes forward movement automatic
        float directionX = Input.GetAxis("Horizontal") * speed;
        //float directionY = 1 * speed;
        float directionY = Input.GetAxis("Vertical") * speed;
        directionX *= Time.deltaTime;
        directionY *= Time.deltaTime;
        transform.Translate(directionX, directionY, 0); //0 could theoretically be swapped for a directionZ
    }
}


//if (Input.GetKeyDown(doAlternateMovement) && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))